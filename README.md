
mini dashboard using coreui vue

### Installation

#### Clone repo

``` bash
# clone the repo
$ git clone https://gitlab.com/muhammad_irfan_sutama/mini-dashboard.git

# go into app's directory
$ cd mini-dashboard

# install app's dependencies
$ npm install
```

#### Usage

``` bash
# serve with hot reload at localhost:8080
npm run serve

# build for production with minification
npm run build

```
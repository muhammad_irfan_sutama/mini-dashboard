import Vue from 'vue'
import Router from 'vue-router'
import { createUser, updateUser } from '../core/services/userCrud'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views - Components
const Forms = () => import('@/views/base/Forms')
const Tables = () => import('@/views/base/Tables')

// Views - Pages
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

// Users
const Users = () => import('@/views/users/Users')
const User = () => import('@/views/users/User')
const UserUpdate = () => import('@/views/users/UpdateUser')
const UserCreate = () => import('@/views/users/CreateUser')

Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/users',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'users',
          meta: {
            label: 'Users'
          },
          component: {
            render(c) {
              return c('router-view')
            }
          },
          children: [
            {
              path: '',
              name: 'Users',
              component: Users
            },
            {
              path: ':id',
              meta: {
                label: 'User Details'
              },
              name: 'User',
              component: User
            },
          ]
        },
        {
          path: 'userCreate',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              name: 'UserCreate',
              component: UserCreate
            },
            {
              path: ':id',
              name: 'UserUpdate',
              component: UserUpdate
            },
          ]
        },
        {
          path: 'userUpdate/:id',
          name: 'UpdateUser',
          component: updateUser
        },
        {
          path: 'base',
          redirect: '/base/cards',
          name: 'Base',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'forms',
              name: 'Forms',
              component: Forms
            },
            {
              path: 'tables',
              name: 'Tables',
              component: Tables
            },
          ]
        },
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/login',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        }
      ]
    }
  ]
}


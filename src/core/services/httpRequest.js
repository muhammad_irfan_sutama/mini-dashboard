import axios from "axios";

export const reqres =  axios.create({
  // baseURLLocal: "http://localhost:3001/api",
  // baseURL: "http://103.25.208.51:3001/pilkada/user/",
  baseURL: "https://reqres.in/api/",
  headers: {
    "Content-type": "application/json",
    "Accept": "application/json"
  }
});
import {reqres} from "./httpRequest";

export const register = (payload) => {
    return reqres.post(`/register`, payload);
};
export const login = (payload) => {
    return reqres.post(`/login`, payload);    
};

export const createUser = (payload) => {
    return reqres.post(`/users`, payload);
};
export const getListUser = (params) => {
    return reqres.get(`/users?${params}`);
    
};
export const getUser = (id) => {
    return reqres.get(`/users/${id}`);  
};

export const deleteUser = (id) => {
    return reqres.delete(`/users/${id}`); 
};
export const updateUser = (payload) => {
    return reqres.put(`/users`, payload);
};